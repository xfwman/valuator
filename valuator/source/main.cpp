#include "../include/io/fileReader.h"

int main(int argc, char* argv[]) {
    //
    // TODO: Enumerate folder with files
    //

    //
    // Process: reader -> parser-> writer
    // TODO: Implement Writer
    //

    try {
        // Create and initialize parser
        Valuator valuator;
        Parser parser(valuator);

        // Create and start reader
        FileReader reader(parser);
        reader.read("data0002.xml");

        return 0;
    }
    catch (std::string e) {
        std::cerr << e;
    }
    catch (...) {
        std::cerr << "Unknown error";
    }

    return 1;
}
