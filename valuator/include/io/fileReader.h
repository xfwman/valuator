#ifndef reader_2FE92588_8FA3_411D_A32D_D5C7282C620F
#define reader_2FE92588_8FA3_411D_A32D_D5C7282C620F

#include <stdio.h>
#include "../parser/parser.h"

class FileReader
{
protected:
	const Parser& parser;

public:
	FileReader(const Parser& p) :
		parser(p)
	{}

protected:
    void readAll(FILE* handle)
    {
        char buffer[128]; // Increase for production
        
        for(;;) {
            int length = (int)fread(buffer, 1, sizeof(buffer), handle);
            if (feof(handle)) {
                parser.parse(buffer, length, 1);
                return;
            }
            else {
                parser.parse(buffer, length, 0);
            }
        }
    }

public:
	void read(const char* name)
	{	
        FILE* handle = NULL;
        errno_t err = fopen_s(&handle, name, "rb");
        if (0 != err)
            throw std::string("Failed to open file.");

        readAll(handle);
        fclose(handle);
	}
};

#endif
