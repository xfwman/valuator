#ifndef multiplication_4E83FDA4_8D47_4B1C_8D5B_8F0C8EEF21DB
#define multiplication_4E83FDA4_8D47_4B1C_8D5B_8F0C8EEF21DB

#include "./addition.h"

template<typename T> class Expression;
typedef Expression<int> tExpression;

template<typename T> class Factor :
	public tInteger
{
public:
	Factor(const XML_Char* name, tElement* pParent) :
		tInteger(name, pParent, tExpression::map)
	{}

public:
	virtual void onValue(int v) {
		value = v;
	}
};

typedef Factor<int> tFactor;

template<typename T> class Multiplication :
	public tElement
{
protected:
	int result;

public:
	Multiplication(const XML_Char* name, tElement* pParent) :
		tElement(name, pParent, Multiplication<T>::map),
		result(INT_MIN)
	{}

public:
	virtual void onEndElement(const XML_Char* name) {
		// TODO: Check if we had the number of required operands
		if (0 < id)
			onResult(id, result);
		else
			pParent->onValue(result);
	}

public:
	virtual void onValue(int v) {
		if (INT_MIN == result) {
			result = v;
		} 
		else {
			result = result * v;
		}
		
	}

public:
	static const tElement::Child map[];
};

template<typename T> const tElement::Child Multiplication<T>::map[] = {
	{"factor", factory<tFactor>},
	{NULL, NULL}
};

typedef Multiplication<int> tMultiplication;

#endif


