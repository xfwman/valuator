#ifndef valuator_58B529F1_046A_4D4B_902D_3A1D677E20B9
#define valuator_58B529F1_046A_4D4B_902D_3A1D677E20B9

#include "./document.h"

class Valuator
{
protected:
	tElement* pStack;

public:
	Valuator() {
		pStack = new tDocument();
	}

	~Valuator() {
		// TODO - Cleanup
	}

public:
	void onStartElement(const XML_Char* name, const XML_Char** atts)
	{
		if (NULL == pStack)
			throw std::string("Unexpected start element.");

		pStack = pStack->onStartElement(name, atts);
	}

	void onData(const char* content, int length)
	{
		if (NULL == pStack)
			throw std::string("Unexpected data");

		pStack->onData(content, length);
	}

	void onEndElement(const XML_Char* name)
	{
		if (NULL == pStack)
			throw std::string("Unexpected end element.");

		if(0 != strcmp(name, pStack->getName()))
			throw std::string("Unexpected end element name.");

		pStack->onEndElement(name);

		// Pop stack and cleanup
		tElement* pTemp = pStack;
		pStack = pStack->pParent;

		delete pTemp;
	}
};

#endif