#ifndef subtraction_424E71B6_963E_4C07_ADCD_B7C59356127A
#define subtraction_424E71B6_963E_4C07_ADCD_B7C59356127A

#include "./integer.h"

template<typename T> class Expression;
typedef Expression<int> tExpression;

template<typename T> class Minuend :
	public tInteger
{
public:
	Minuend(const XML_Char* name, tElement* pParent) :
		tInteger(name, pParent, tExpression::map)
	{}

	virtual void onEndElement(const XML_Char* name) {
		if (INT_MIN == value) {
			data[offset] = 0;
			value = atoi(data); // TODO: Check if valid
		}
		pParent->onMinuend(value);
	}
};

typedef Minuend<int> tMinuend;

template<typename T> class Subtrahend :
	public tInteger
{
public:
	Subtrahend(const XML_Char* name, tElement* pParent) :
		tInteger(name, pParent, tExpression::map)
	{}

	virtual void onEndElement(const XML_Char* name) {
		if (INT_MIN == value) {
			data[offset] = 0;
			value = atoi(data); // TODO: Check if valid
		}
		pParent->onSubtrahend(value);
	}
};

typedef Subtrahend<int> tSubtrahend;

template<typename T> class Subtraction :
	public tElement
{
protected:
	int minuend;
	int subtrahend;

public:
	Subtraction(const XML_Char* name, tElement* pParent) :
		tElement(name, pParent, Subtraction<T>::map),
		minuend(INT32_MIN), subtrahend(INT32_MIN)
	{}

public:
	virtual void onEndElement(const XML_Char* name) {
		// TODO: Check if both operands are set
		int result = minuend - subtrahend;
		if (0 < id)
			onResult(id, result);
		else
			pParent->onValue(result);
	}

public:
	virtual void onMinuend(int v) {
		minuend = v; // TODO: check if already set
	}

	virtual void onSubtrahend(int v) {
		subtrahend = v; // TODO: check if already set
	}

public:
	static const tElement::Child map[];
};

template<typename T> const tElement::Child Subtraction<T>::map[] = {
	{"minuend", factory<tMinuend>},
	{"subtrahend", factory<tSubtrahend>},
	{NULL, NULL}
};

typedef Subtraction<int> tSubtraction;

#endif
