#ifndef document_ECCBE33B_34FB_4384_A663_512139251C0D
#define document_ECCBE33B_34FB_4384_A663_512139251C0D

#include "./expression.h"

template <typename T> class Document :
	public tElement
{
public:
	Document() :
		tElement(NULL, NULL, Document<T>::map)
	{}

public:
	static const tElement::Child map[];
};

template<typename T> const tElement::Child Document<T>::map[] = {
		{ "expressions", factory<tRoot>},
		{NULL, NULL}
};

typedef Document<int> tDocument;

#endif