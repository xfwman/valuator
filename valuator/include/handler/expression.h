#ifndef expression_C1023A9D_E511_4D1F_84F0_6D654A60624F
#define expression_C1023A9D_E511_4D1F_84F0_6D654A60624F

#include "./addition.h"
#include "./division.h"
#include "./multiplication.h"
#include "./subtraction.h"

template<typename T> class Expression :
	public tElement
{
public:
	Expression(const XML_Char* name, tElement* pParent) :
		tElement(name, pParent, Expression<T>::map)
	{}

public:
	static const tElement::Child map[];
};

template<typename T> const tElement::Child Expression<T>::map[] = {
	{"addition", factory<tAddition>},
	{"division", factory<tDivision>},
	{"multiplication", factory<tMultiplication>},
	{"subtraction", factory<tSubtraction>},
	{NULL, NULL}
};

typedef Expression<int> tExpression;

template<typename T> class Root :
	public tExpression
{
public:
	Root(const XML_Char* name, tElement* pParent) :
		tExpression(name, pParent)
	{
		std::cout << "<expression>" << std::endl;
	}

public:
	virtual void onEndElement(const XML_Char* name) {
		tElement::onEndElement(name);
		std::cout << "</expression>" << std::endl;
	}

public:
	virtual void onResult(int id, int v) {
		std::cout << "\t<result id=\"" << id << "\">" << v << "</result>" << std::endl;
	}
};

typedef Root<int> tRoot;

#endif
