#ifndef addition_01B0BBBE_1ADD_41FB_95F9_7EF506B18F91
#define addition_01B0BBBE_1ADD_41FB_95F9_7EF506B18F91

#include "./integer.h"

template<typename T> class Expression;
typedef Expression<int> tExpression;

template<typename T> class Item :
	public tInteger
{
public:
	Item(const XML_Char* name, tElement* pParent) :
		tInteger(name, pParent, tExpression::map)
	{}

public:
	virtual void onValue(int v) {
		value = v;
	}
};

typedef Item<int> tItem;

template<typename T> class Addition :
	public tElement
{
protected:
	int sum;

public:
	Addition(const XML_Char* name, tElement* pParent) :
		tElement(name, pParent, Addition<T>::map),
		sum(0)
	{}

public:
	virtual void onEndElement(const XML_Char* name) {
		// TODO: Check if we had the number of required operands
		if (0 < id)
			onResult(id, sum);
		else
			pParent->onValue(sum);
	}

public:
	virtual void onValue(int v) {
		sum += v;
	}

public:
	static const tElement::Child map[];
};

template<typename T> const tElement::Child Addition<T>::map[] = {
	{"item", factory<tItem>},
	{NULL, NULL}
};

typedef Addition<int> tAddition;

#endif

