#ifndef element_C87980C9_3DD8_435D_B5E4_5321AD300185
#define element_C87980C9_3DD8_435D_B5E4_5321AD300185

#include "../misc/common.h"

template<typename T> class Element
{
public:
	typedef Element<T> tThis;
	typedef tThis* (*tFactory)(const XML_Char* name, tThis* pParent);

	struct Child {
		const char* name;
		tFactory factory;
	};

public:
	const Child* pChildren;
	tThis* pParent;

protected:
	char name[64];
	int id;

public:
	Element(const XML_Char* n, tThis* p, const Child* c) :
		id(-1), pChildren(c), pParent(p)
	{
		if (NULL != n) {
			errno_t err = strcpy_s(name, sizeof(name), n);
			if (0 != err)
				throw std::string("Unexpected element name.");
		}
	}

	virtual ~Element()
	{}

protected:
	tFactory getFactory(const XML_Char* name) {
		if (NULL == pChildren)
			return NULL;

		const Child* p = pChildren;
		while (NULL != p->name) {
			if (0 == strcmp(name, p->name))
				return p->factory;
			p++;
		}

		return NULL;
	}

public:
	const char* getName() {
		return name;
	}

public:
	virtual tThis* onStartElement(const XML_Char* name, const XML_Char** atts) {
		tFactory factory = getFactory(name);
		if (NULL == factory)
			throw std::string("Invalid element.");

		tThis* pElement = factory(name, this);
		if(NULL == pElement)
			throw std::string("Unable to create element.");

		while (NULL != *atts) {
			if (0 == strcmp("id", *atts++)) {
				pElement->id = atoi(*atts);
				break;
			}
			atts++;
		}

		return pElement;
	}

	virtual void onData(const char* content, int length) {
		skipSpace(content, length);
		if (0 < length)
			throw std::string("Invalid element data");
	}

	virtual void onEndElement(const XML_Char* name) {
	}

public:
	virtual void onResult(int id, int v) {
		if(NULL == pParent)
			throw std::string("Unexpected parent");

		pParent->onResult(id, v);
	}

	virtual void onValue(int v) {
		throw std::string("Unexpected value");
	}

	virtual void onDividend(int v) {
		throw std::string("Unexpected value");
	}

	virtual void onDivisor(int v) {
		throw std::string("Unexpected value");
	}

	virtual void onMinuend(int v) {
		throw std::string("Unexpected value");
	}

	virtual void onSubtrahend(int v) {
		throw std::string("Unexpected value");
	}
};

typedef Element<int> tElement;

template<typename T> tElement* factory(const XML_Char* name, tElement* parent) {
	return new T(name, parent);
}

#endif

