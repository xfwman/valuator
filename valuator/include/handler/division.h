#ifndef division_C469FF8E_AFE5_4E59_9DEE_F78A28AD87E5
#define division_C469FF8E_AFE5_4E59_9DEE_F78A28AD87E5

#include "./integer.h"

template<typename T> class Expression;
typedef Expression<int> tExpression;

template<typename T> class Dividend :
	public tInteger
{
public:
	Dividend(const XML_Char* name, tElement* pParent) :
		tInteger(name, pParent, tExpression::map)
	{}

	virtual void onEndElement(const XML_Char* name) {
		if (INT_MIN == value) {
			data[offset] = 0;
			value = atoi(data); // TODO: Check if valid
		}
		pParent->onDividend(value);
	}
};

typedef Dividend<int> tDividend;

template<typename T> class Divisor :
	public tInteger
{
public:
	Divisor(const XML_Char* name, tElement* pParent) :
		tInteger(name, pParent, tExpression::map)
	{}

	virtual void onEndElement(const XML_Char* name) {
		if (INT_MIN == value) {
			data[offset] = 0;
			value = atoi(data); // TODO: Check if valid
		}
		pParent->onDivisor(value);
	}

public:
	virtual void onValue(int v) {
		value = v;
	}
};

typedef Divisor<int> tDivisor;

template<typename T> class Division :
	public tElement
{
protected:
	int dividend;
	int divisor;

public:
	Division(const XML_Char* name, tElement* pParent) :
		tElement(name, pParent, Division<T>::map),
		dividend(INT_MIN), divisor(INT_MIN)
	{}

public:
	virtual void onEndElement(const XML_Char* name) {
		// TODO: Check if both operands are set
		int result = dividend / divisor;
		if (0 < id)
			onResult(id, result);
		else
			pParent->onValue(result);
	}

public:
	virtual void onDividend(int v) {
		dividend = v;
	}

	virtual void onDivisor(int v) {
		divisor = v;
	}

public:
	static const tElement::Child map[];
};

template<typename T> const tElement::Child Division<T>::map[] = {
	{"dividend", factory<tDividend>},
	{"divisor", factory<tDivisor>},
	{NULL, NULL}
};

typedef Division<int> tDivision;

#endif

