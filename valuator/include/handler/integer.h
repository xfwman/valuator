#ifndef integer_0C27A766_BA3A_43F6_A29A_5FB26297BA94
#define integer_0C27A766_BA3A_43F6_A29A_5FB26297BA94

#include "./element.h"

template<typename T> class Integer :
	public tElement
{
protected:
	char data[32];
	int offset;
	int value;

public:
	Integer(const XML_Char* name, tElement* pParent, const Child* children) :
		tElement(name, pParent, children),
		offset(0), value(INT_MIN)
	{}

public:
	virtual void onData(const char* content, int length) {
		errno_t err = memcpy_s((void*)(data + offset), sizeof(data) - offset - 1, content, length);
		if (0 != err)
			throw std::string("Unexpected data size.");
		offset += length;
	}

	virtual void onEndElement(const XML_Char* name) {
		if (INT_MIN == value) {
			data[offset] = 0;
			value = atoi(data); // TODO: Test if value is OK
		}
		pParent->onValue(value);
	}
};

typedef Integer<int> tInteger;

#endif
