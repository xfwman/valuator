#ifndef parser_1C99DFAC_48F9_43DE_A04C_D662CCAC695A
#define parser_1C99DFAC_48F9_43DE_A04C_D662CCAC695A

#include "../handler/valuator.h"

class Parser
{
protected:
	static void XMLCALL startElement(void* userData, const XML_Char* name, const XML_Char** atts)
	{
		Parser* pThis = (Parser*)userData;
		pThis->onStartElement(name, atts);
	}

	static void XMLCALL onData(void* userData, const char* content, int length)
	{
		Parser* pThis = (Parser*)userData;
		pThis->onData(content, length);
	}

	static void XMLCALL endElement(void* userData, const XML_Char* name) 
	{
		Parser* pThis = (Parser*)userData;
		pThis->onEndElement(name);
	}

protected:
	std::string error;
	Valuator& valuator;
	XML_Parser parser;

public:
	Parser(Valuator& v) :
		valuator(v)
	{
		parser = XML_ParserCreate(NULL);
		if (NULL == parser)
			throw  std::string("Failed to create parser");

		XML_SetUserData(parser, this);
		XML_SetElementHandler(parser, Parser::startElement, Parser::endElement);
		XML_SetCharacterDataHandler(parser, onData);
	}	

	~Parser()
	{	
		if (NULL != parser)
			XML_ParserFree(parser);
	}

protected:
	void onStartElement(const XML_Char* name, const XML_Char** atts)
	{
		if (!error.empty())
			return;

		try {
			valuator.onStartElement(name, atts);
		}
		catch (std::string e) {
			error = e;
		}
		catch (...) {
			error = "Unknown error";
		}
	}

	void onData(const char* content, int length)
	{
		if (!error.empty())
			return;

		try {
			valuator.onData(content, length);
		}
		catch (std::string e) {
			error = e;
		}
		catch (...) {
			error = "Unknown error";
		}
	}

	void onEndElement(const XML_Char* name)
	{
		if (!error.empty())
			return;

		try {
			valuator.onEndElement(name);
		}
		catch (std::string e) {
			error = e;
		}
		catch (...) {
			error = "Unknown error";
		}
	}

public:
	void parse(char* buffer, int length, int complete) const
	{
		// Forward data to expat
		XML_Status s = XML_Parse(parser, buffer, length, complete);
		if (XML_STATUS_OK != s)
			throw std::string("Parser faild.");

		// Rethrow previous errors
		if (!error.empty())
			throw error;
	}
};

#endif
