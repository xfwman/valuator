#ifndef common_58B529F1_046A_4D4B_902D_3A1D677E20B9
#define common_58B529F1_046A_4D4B_902D_3A1D677E20B9

#include <iostream>
#include <expat.h>

inline void skipSpace(const char*& c, int& l) {
	while (0 < l) {
		if (!isspace(*c)) 
			return;

		c++; l--;
	}
}

#endif
